import requests
import socket
import yaml
import json
from pathlib import Path
writehosts = "writehosts"           # Имя файла yaml/json куда пишем результаты
configfile = "services_conf.yaml"   # Конфиг со списками серверов для проверки формата "- domain.local: 0.0.0.0"
sdisc = {}
csrv = {}
data = []
firsttime = False
mismatch = False
def esc(code):
    return f'\033[{code}m'

def convertlist2dic(serv):
    c2d = {}
    for element in serv:
        items = list(element.items())
        c2d[items[0][0]] = items[0][1]
    return c2d

try:
    srv = yaml.safe_load(Path(configfile).read_text())  # читаем файл конфигурации (список сервисов)
except:
    print(f"Please create {configfile}")
    quit()
try:
    csrv = yaml.safe_load(Path(writehosts + ".yaml").read_text())
except:
    print("Can't open result json/yaml creating files, please start again script")
    firsttime = True

sdisc = convertlist2dic(srv)
csrv = convertlist2dic(csrv)

for i in sdisc:
    response = requests.head("https://" + str(i))
    hostname = str(i)
    ip = socket.gethostbyname(hostname)
    if not firsttime:
        rip = csrv[i]
    else:
        rip = 0
    if rip != ip and not firsttime:
        print(esc('31;1;4') + "[ERROR]" + esc('0;0;0') + " {0} IP mismatch: {1} {2}".format(str(i), rip, ip))
        mismatch = True
    else:
        mismatch = False
    data.append({hostname: ip})
    with open(writehosts + ".json", 'w') as jf:
        json_data = json.dumps(data)
        jf.write(json_data)
    with open(writehosts + ".yaml", 'w') as yf:
        yaml_data = yaml.dump(data)
        yf.write(yaml_data)
    if firsttime:
        continue
    if (response.status_code == 200 or response.status_code == 301 or response.status_code == 302) and not mismatch:
        print("OK ", str(i), ip)
    else:
        if not mismatch:
            print(f"NOT OK: HTTP response code {response.status_code} {ip}")