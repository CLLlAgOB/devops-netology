Роли
=========
Создаем роли:
ansible-galaxy init clickhouse
ansible-galaxy init lighthouse
ansible-galaxy init vector

 Описание ролей: clickhouse.
- Установка:
  - clickhouse-client
  - clickhouse-server
  - clickhouse-common-static
- Создаётся БД
- Создаётся таблица для логов 
- Создаётся пользователь для записи в БД
- Конфигурируется clickhouse-server для работы внешних подключений 

 Описание ролей: vector.
- Установка: rpm пакета
- Создание файла конфигурации
- Создание каталогов 
- Запуск службы vector

 Описание ролей: lighthouse.
- Установка nginx
- конфигурирование nginx
- Установка git
- копирование из гита
- создание конфига
- перезапуск nginx

Переменные
--------------

Переменные для установки доступа
default/main.yml:
```yaml
clickhouse_user: netology
clickhouse_password: netology
```

Переменные для установки необходимых пакетов и конфигурационных файлов clickhouse
vars/main.yml
```yaml
clickhouse_version: "22.3.3.44"
clickhouse_packages:
  - clickhouse-client
  - clickhouse-server
  - clickhouse-common-static
clickhouse_config_path: /etc/clickhouse-server/config.xml
clickhouse_users_path: /etc/clickhouse-server/users.xml
```

Переменные для установки необходимых пакетов и конфигурирования файлов vector
/vars/main.yml
```yaml
vector_version: vector-0.21.1-x86_64-unknown-linux-musl
vector_config_path: /etc/vector/vector.yaml
vector_config:
  sources:
    internal_logs:
      type: internal_logs
    local_logs:
      type: file
      include:
        - /var/log/**/*.log
      read_from: beginning
  sinks:
    local_to_clickhouse:
      type: clickhouse
      inputs:
        - local_logs
      database: logs
      endpoint: http://{{ hostvars['clickhouse-01'].ansible_host }}:8123
      table: vector_internal_logs
      auth:
        user: "{{ clickhouse_user }}"
        password: "{{ clickhouse_password }}"
        strategy: basic
      compression: gzip
      healthcheck: false
      skip_unknown_fields: true
```
Переменные для установки необходимых пакетов и конфигурирования файлов lighthouse
/vars/main.yml
```yaml
lighthouse_vcs: https://github.com/VKCOM/lighthouse.git
lighthouse_dir: /var/lib/lighthouse
lighthouse_access_log_name: lighthouse_access
nginx_user_name: root
worker_processes: auto
worker_connections: 2048
client_max_body_size: 512M
```
