import requests
import re
import socket
def esc(code):
    return f'\033[{code}m'
array_urls = ("https://mail.ru", "https://ok.com", "https://drive.google.com", "http://ya.ru")

for i in array_urls:
    try:
        response = requests.head(str(i))
        hostname = re.sub(r'^https?:\/\/', '', str(i))
        ip = socket.gethostbyname(hostname)
        try:
            f = open(hostname, "r")
        except Exception as e:
            f = open(hostname, "w")
            f.write(ip)
            f.close()
        f = open(hostname, "r")
        rip = f.readline()
        if rip != ip:
            print(esc('31;1;4') + "[ERROR]" + esc('0;0;0') + " {0} IP mismatch: {1} {2}".format(str(i), rip, ip))
        f.close()
        f = open(hostname, "w")
        f.write(ip)
        f.close()
#        print(str(i))
    except Exception as e:
        print(esc('31;1;4') + "NOT OK: " + esc('0;0;0'), str(i), ip)
    else:
        if response.status_code == 200 or response.status_code == 301 or response.status_code == 302:
            print("OK ", str(i), ip)
        else:
            print(f"NOT OK: HTTP response code {response.status_code} {ip}")
