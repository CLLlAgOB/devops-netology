[all]
%{ for host, host_data in hosts ~}
${host} ansible_host=${host_data.ansible_host} ansible_user=${username} ansible_ssh_private_key_file=${ssh_public}
%{ endfor ~}

[all:children]
%{ for group, data in groups ~}
${group}
%{ endfor ~}

%{ for group, group_data in groups ~}
[${group}]
%{ for host, host_data in group_data.hosts ~}
${host} ansible_host=${host_data.ansible_host} ansible_user=${username} ansible_ssh_private_key_file=${ssh_public}
%{ endfor ~}
%{ endfor ~}
