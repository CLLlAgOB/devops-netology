data "template_file" "instance_userdata" {
  template = file("linux.tpl")
  vars = {
    env        = "perf"
    username   = "ec2-user"
    ssh_public = file("/home/aleksandr/.keys/public.key")
  }
}

data "yandex_compute_image" "ubuntu_2004" {
  family = "ubuntu-2004-lts"
}

data "yandex_compute_image" "ubuntu_image" {
  family = "ubuntu-2004-lts"
}