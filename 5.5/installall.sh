#!/bin/bash
cd ./terraform
#terraform plan
terraform apply -auto-approve
rm ./inventory.yaml
echo "Wait 40 sec for start VM"
sleep 40
echo "Continue Installation"
cd ../ansible/docker/
ansible-playbook -i ../../terraform/inventory.ini playbook.yml