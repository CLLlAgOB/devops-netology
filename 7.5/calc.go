package main

import (
    "fmt"
    "math"
)

func main() {
    fmt.Print("перевод метров в футы введите метры: ")
    var input float64
    fmt.Scanf("%f", &input)
    output := input * 0.3048
    fmt.Println("футов -",math.Round(output*100) / 100) // вывод с округлением
}