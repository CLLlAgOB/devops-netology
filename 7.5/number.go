package main
import "fmt"

func main() {

    x := []int{48,96,86,68,57,82,63,70,37,34,83,27,19,97,9,17,2}
        // First element as smallest number assumption
    smallestNumber := x[0]
        // Iterate array using rage loop
    for _,
    element := range x {
        // check for the smallest number
        if element < smallestNumber {
            smallestNumber = element

        }
    }
    fmt.Println("Наименьшее число", smallestNumber)
}