# Домашнее задание к занятию "4.3. Языки разметки JSON и YAML"


## Обязательная задача 1
Мы выгрузили JSON, который получили через API запрос к нашему сервису:
```yaml
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : "7175"
        },
        { "name" : "second",
        "type" : "proxy",
        "ip" : "71.78.22.43"
        }
    ]
}
```
  Нужно найти и исправить все ошибки, которые допускает наш сервис

``
Ошибки исправил, нехватало кавычек, если рассматривать с точки зрения синтаксиса. 
Если с точки зрения данных то в первом элементе ip явно косяк.
``
## Обязательная задача 2
В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML файлов, описывающих наши сервисы. Формат записи JSON по одному сервису: `{ "имя сервиса" : "его IP"}`. Формат записи YAML по одному сервису: `- имя сервиса: его IP`. Если в момент исполнения скрипта меняется IP у сервиса - он должен так же поменяться в yml и json файле.

### Ваш скрипт:
```python
import requests
import socket
import yaml
import json
from pathlib import Path
writehosts = "writehosts"           # Имя файла yaml/json куда пишем результаты
configfile = "services_conf.yaml"   # Конфиг со списками серверов для проверки формата "- domain.local: 0.0.0.0"
sdisc = {}
csrv = {}
data = []
firsttime = False
mismatch = False
def esc(code):
    return f'\033[{code}m'

def convertlist2dic(serv):
    c2d = {}
    for element in serv:
        items = list(element.items())
        c2d[items[0][0]] = items[0][1]
    return c2d

try:
    srv = yaml.safe_load(Path(configfile).read_text())  # читаем файл конфигурации (список сервисов)
except:
    print(f"Please create {configfile}")
    quit()
try:
    csrv = yaml.safe_load(Path(writehosts + ".yaml").read_text())
except:
    print("Can't open result json/yaml creating files, please start again script")
    firsttime = True

sdisc = convertlist2dic(srv)
csrv = convertlist2dic(csrv)

for i in sdisc:
    response = requests.head("https://" + str(i))
    hostname = str(i)
    ip = socket.gethostbyname(hostname)
    if not firsttime:
        rip = csrv[i]
    else:
        rip = 0
    if rip != ip and not firsttime:
        print(esc('31;1;4') + "[ERROR]" + esc('0;0;0') + " {0} IP mismatch: {1} {2}".format(str(i), rip, ip))
        mismatch = True
    else:
        mismatch = False
    data.append({hostname: ip})
    with open(writehosts + ".json", 'w') as jf:
        json_data = json.dumps(data)
        jf.write(json_data)
    with open(writehosts + ".yaml", 'w') as yf:
        yaml_data = yaml.dump(data)
        yf.write(yaml_data)
    if firsttime:
        continue
    if (response.status_code == 200 or response.status_code == 301 or response.status_code == 302) and not mismatch:
        print("OK ", str(i), ip)
    else:
        if not mismatch:
            print(f"NOT OK: HTTP response code {response.status_code} {ip}")
```

### Вывод скрипта при запуске при тестировании:
```
aleksandr@R2D2:~/Documents/netology/devops-netology/python$ python3 4.py
Can't open result json/yaml creating files, please start again script
aleksandr@R2D2:~/Documents/netology/devops-netology/python$ python3 4.py
OK  mail.ru 94.100.180.200
[ERROR] ok.com IP mismatch: 99.83.190.102 75.2.70.75
OK  drive.google.com 216.58.209.14
OK  ya.ru 87.250.250.242
aleksandr@R2D2:~/Documents/netology/devops-netology/python$ python3 4.py
[ERROR] mail.ru IP mismatch: 94.100.180.200 217.69.139.202
[ERROR] ok.com IP mismatch: 75.2.70.75 99.83.190.102
OK  drive.google.com 216.58.209.14
OK  ya.ru 87.250.250.242

```

### json-файл(ы), который(е) записал ваш скрипт:
```json
[{"mail.ru": "217.69.139.202"}, {"ok.com": "99.83.190.102"}, {"drive.google.com": "216.58.209.14"}, {"ya.ru": "87.250.250.242"}]
```

### yml-файл(ы), который(е) записал ваш скрипт:
```yaml
- mail.ru: 217.69.139.202
- ok.com: 99.83.190.102
- drive.google.com: 216.58.209.14
- ya.ru: 87.250.250.242
```

## Дополнительное задание (со звездочкой*) - необязательно к выполнению

Так как команды в нашей компании никак не могут прийти к единому мнению о том, какой формат разметки данных использовать: JSON или YAML, нам нужно реализовать парсер из одного формата в другой. Он должен уметь:
   * Принимать на вход имя файла
   * Проверять формат исходного файла. Если файл не json или yml - скрипт должен остановить свою работу
   * Распознавать какой формат данных в файле. Считается, что файлы *.json и *.yml могут быть перепутаны
   * Перекодировать данные из исходного формата во второй доступный (из JSON в YAML, из YAML в JSON)
   * При обнаружении ошибки в исходном файле - указать в стандартном выводе строку с ошибкой синтаксиса и её номер
   * Полученный файл должен иметь имя исходного файла, разница в наименовании обеспечивается разницей расширения файлов

### Ваш скрипт:
```python
???
```

### Пример работы скрипта:
???
