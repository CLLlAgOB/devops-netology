variable "zone" {
  type = string
  default = "ru-central1-a"
}
variable "folder_id" {
  type = string
  default = "b1g4152c4kqrduv0rh7d"
}
variable "subnet" {
  type = string
  default = "e9b6rc2t9motmhph0bgq"
}
