# 08-ansible-02-playbook

# Инфраструктура:

1. Сервер `clickhouse-01` для сбора логов.
2. Сервер `vector-01`, генерирующий и обрабатывающий логи.\
3. 
### playbook

Playbook производит установку и настройку следующих приложений на указанных серверах.


- ### Clickhouse

  - установка `clickhouse`
  - настройка удаленных подключений к приложению
  - создание базы данных и таблицы в ней если она отсуствует. 


- ### Vector

  - установка `vector`
  - изменение конфига приложения для отправки логов на сервер `clickhouse-01`



### Какие у него есть параметры 

- IP Хоста и группы `prod.yml`
- В `group_vars\clickhouse\vars.yml` ( clickhouse_version версия и компоненты )
- В `group_vars\vector\vars.yml` (Версия)


## Variables

Через group_vars можно задать следующие параметры:
- `clickhouse_version`, `vector_version` - версии устанавливаемых приложений;
- `clickhouse_packages` - компоненты кликхауса 

## Tags

- `clickhouse` производит развертывание сервера `clickhouse-01`;
- `clickhouse_db` производит конфигурацию базы данных и таблицы;
- `vector` производит полную конфигурацию сервера `vector-01`;
- `vector_config` обновит конфиг вектора;


---

- Для изменения конфигурации `vector` достаточно использовать тэг `vector_config`

    ```console
    ansible-playbook -i inventory/prod.yml site.yml --tags vector_config
    ```

- для создания новой базы данных и записи логов в нее необходимо использовать тэги `clickhouse_db` и `vector_config`

    ```console
    ansible-playbook -i inventory/prod.yml site.yml --tags clickhouse_db --tags vector_config
    ```

Для полной установки инфраструктуры указывать тэги не требуется.
