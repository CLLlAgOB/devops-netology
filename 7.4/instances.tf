module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  for_each = local.web_instance_for_each_map[terraform.workspace]

  name = "Netology-${each.key}"

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = local.web_instance_type_map[terraform.workspace]
  key_name               = "user1"
  monitoring             = true

  tags = {
    Terraform   = "true"
#    Environment = "dev"
    Name = "ubuntu_count_${terraform.workspace}_${each.key}"
  }
}