#!/usr/bin/env python3
import os

bash_command = ["cd ~/Documents/netology/devops-netology/", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
full_path = os.popen("pwd").read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        full_string = full_path.rstrip('\n') + "/" + prepare_result
        print(full_string)
