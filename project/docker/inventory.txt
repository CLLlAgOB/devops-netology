[monitorserver]
dbserver-01   ansible_host=130.193.37.198  ansible_user=ec2-user  ansible_ssh_private_key_file=/home/aleksandr/.keys/private.key
[nodeservers]
worker-01  ansible_host=130.193.49.119  ansible_user=ec2-user  ansible_ssh_private_key_file=/home/aleksandr/.keys/private.key
worker-02  ansible_host=130.193.48.147  ansible_user=ec2-user  ansible_ssh_private_key_file=/home/aleksandr/.keys/private.key