#!/usr/bin/env python3
import os
import sys

try:
    sys.argv[1]
except IndexError:
    path = "cd " + os.getcwd()
else:
    path = "cd " + sys.argv[1]
bash_command = [path, "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
full_path = os.popen("pwd").read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1 or result.find('изменено') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        full_string = full_path.rstrip('\n') + "/" + prepare_result
        print(full_string)
